import 'package:flutter/material.dart';
import 'package:swiper/src/pages/swiper_targe.dart';


void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      
      title: 'Marketplace',
      theme: ThemeData(

        primarySwatch: Colors.blue,
      ),
      home: SwiperTarge(),
    );
  }
  
}
