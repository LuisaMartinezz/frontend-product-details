import 'package:http/http.dart' as http;
import 'dart:convert';

class HttpClient {
  Future<http.Response> get(uri) {
    return  http.get(uri);
  }
  Future<http.Response> post(uri,body) {
    body = json.encode(body);
    return  http.post(uri,headers: {"Content-Type": "application/json"},
      body:body
    );
  }
  Future<http.Response> put(uri,body) {
    body = json.encode(body);
    return  http.put(uri,headers: {"Content-Type": "application/json"},body:body
    );
  }
  Future<http.Response> delete(uri) {
    return  http.delete(uri);
  }
}

