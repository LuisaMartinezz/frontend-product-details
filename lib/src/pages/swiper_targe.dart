import 'package:flutter/material.dart';
import 'package:swiper/src/Widget/card.dart';
import 'package:swiper/src/conection/conection.dart'; 

class SwiperTarge extends StatefulWidget {
 
  SwiperTarge() {
   
  }

  @override
  _SwiperTargeState createState() => _SwiperTargeState();
}

class _SwiperTargeState extends State<SwiperTarge> {

   bool isBuy;
   String title;
   String description;
   Conection conection;
   int cost;
  _SwiperTargeState(){
    isBuy= false;
     conection =  Conection();
    conection.getProduct(6).then((res){
      print(res);
      setState(() {
        cost = res['cost'];
        title = res['nameProduct'];
        description = res['characteristics'];
        isBuy=true;
      });
    });

  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          children: <Widget>[
            Icon(Icons.arrow_back),
            // Container(
            //   padding: EdgeInsets.only(left: 12),
            //   child: Text('producto'),
            // )
          ],
        ),
        actions: <Widget>[
          Container(
            child: Row(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(right: 15),
                  child: Icon(Icons.shopping_cart),
                )
              ],
            ),
          )
        ],
      ),
      body: Container(
        padding: EdgeInsets.only(top: 30, bottom: 15),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              _swiperTarge(),
              _title(),
              _value(),
              _text(),
              _description(),
              if(isBuy) Container(
                padding: EdgeInsets.only(left: 120, right: 120),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: _button(),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _swiperTarge() {
    return CardSwiperWidget(
      imagenes: [
        'assets/celulares/celular.jpg',
        'assets/celulares/redmi.jpg',
        'assets/celulares/venta.jpg'
      ],
    );
  }

  Widget _title() {
    return Container(
      padding: EdgeInsets.only(top: 30, bottom: 15),
      child: Center(
        child: Text(
          '$title',
          style: TextStyle(
            fontSize: 20,
            color: Colors.black,
          ),
        ),
      ),
    );
  }

  Widget _value() {
    return Container(
      padding: EdgeInsets.only(right: 130),
      child: Center(
        child: Text(
        'costo: $cost',
        style: TextStyle(
          fontSize: 20,
          color: Colors.black,
        ),
        ),
        
      ),
    );
  }

  Widget _text() {
    return Container(
      padding: EdgeInsets.fromLTRB(1, 5, 180, 5),
      child: Center(
        child: Container(
          padding: EdgeInsets.only(left: 10.0, bottom: 10),
          child: Text(
            'Descripción',
            style: TextStyle(
              fontSize: 19,
            ),
          ),
        ),
      ),
    );
  }

  Widget _description() {
    return Container(
      padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
      child: Center(
          child: Container(
        padding: EdgeInsets.only(left: 30, right: 30, bottom: 10),
        child: Text(
          '$description',
          textAlign: TextAlign.justify,
          style: TextStyle(
            fontSize: 15,
          ),
        ),
      )),
    );
  }

  Widget _icon() {
    return Icon(
      Icons.shopping_cart,
    );
  }

  Widget _button() {
    return MaterialButton(
     
      minWidth: 50.0,
      height: 30.0,
        onPressed:(){},
      color: Colors.lightBlue,
      child: Text('Comprar', style: TextStyle(color: Colors.white)),
    );
  }
}
