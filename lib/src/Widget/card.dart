import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

class CardSwiperWidget extends StatelessWidget {
  final List<dynamic> imagenes;

  CardSwiperWidget({@required this.imagenes});

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    
  

    return Container(
      padding: const EdgeInsets.only(top: 10, left: 0),
      child: Swiper(
        layout: SwiperLayout.STACK,
        itemWidth: screenSize.width * 0.6,
        itemHeight: screenSize.height * 0.4,
        itemBuilder: (BuildContext context, int index) {
          return ClipRRect(
              borderRadius: BorderRadius.circular(20.0),
              child: FadeInImage(
                image: AssetImage(imagenes[index]),
                placeholder: AssetImage('assets/icono/img1.png'),
                fit: BoxFit.cover,
              ));
        },
        itemCount: imagenes.length,
        //pagination: new SwiperPagination(),
        //control: new SwiperControl(),
      ),
    );
  }

  

}

